package com.capacity.open.webservice.config;


import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cn.capacity.open.ws.onlineuserser.OnlineUserSer;
import cn.capacity.open.ws.onlineuserser.OnlineUserSerImpl;

 
@Configuration
public class CxfConfig {
	
	//http://127.0.0.1:8089/uniServiceAccess/services/O2oUserSer?wsdl
	@Bean
	public ServletRegistrationBean cxfServlet() {
		return new ServletRegistrationBean(new CXFServlet(), "/services/*");
	}

	@Bean(name = Bus.DEFAULT_BUS_ID)
	public SpringBus springBus() {
		return new SpringBus();
	}
	@Bean
	public OnlineUserSer onlineUserSer() {
		
		OnlineUserSer o2OUserSer = new OnlineUserSerImpl() ;
		return o2OUserSer;
	}
	@Bean
	public Endpoint endpoint() {
		EndpointImpl endpoint = new EndpointImpl(springBus(), onlineUserSer());
		endpoint.publish("/OnlineUserSer");
		return endpoint;
	}
}